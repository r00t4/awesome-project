package lib

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"

	_ "github.com/lib/pq"
)

var repoQueries = []string{
	`CREATE TABLE IF NOT EXISTS items
( 
	title TEXT,
	date TEXT,
	descr TEXT,
	UNIQUE (title, date)
);`,
}

type ItemsStore interface {
	Add(items []Item) ([]Item, error)

	GetByTitles(base, quoted string) ([]ItemStat, error)

	GetByDateAndTitle(date, title string) (*Item, error)

	GetByDate(date string) ([]Item, error)
}

type store struct {
	db *sql.DB
}

func NewItemsStore(connStr string) (ItemsStore, error) {
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	for _, q := range repoQueries {
		_, err = db.Exec(q)
		if err != nil {
			log.Println(err)
		}
	}

	store := &store{db}

	return store, nil
}

func (s *store) Add(items []Item) ([]Item, error) {
	if len(items) < 1 {
		return nil, errors.New("nothing to insert")
	}
	query := "INSERT INTO items (title, date, descr) "+
		"VALUES "
	var values []interface{}
	cnt := 1
	for i, item := range items {
		parts := []string{"$" +strconv.Itoa(cnt), "$" +strconv.Itoa(cnt+1), "$" +strconv.Itoa(cnt+2)}
		cnt+=3
		query = query + "(" + strings.Join(parts, " , ") + ")"
		values = append(values, item.Title, item.PubDate, item.Description)
		if i < len(items)-1 {
			query += ", "
		}
	}

	fmt.Println(query)
	result, err := s.db.Exec(
		query +" on conflict do nothing", values...)
	if err != nil {
		return nil, err
	}
	_, err = result.RowsAffected()
	if err != nil {
		return nil, err
	}
	return items, nil
}

func (s *store) GetByTitles(base, quoted string) ([]ItemStat, error) {
	items := []ItemStat{}
	var values []interface{}
	query := ""
	if base != "" && quoted != "" {
		query += "select first.date, first.descr, second.descr from items as first " +
			"INNER JOIN items as second ON second.date = first.date where first.title = $1 and second.title = $2"
		values = append(values, base, quoted)
	} else if base == "" && quoted == "" {
		return nil, errors.New("empty parameters")
	} else if quoted == "" {
		query = "SELECT date, descr, 1 FROM items WHERE title = $1"
		values = append(values, base)
	} else {
		query = "SELECT date, 1, descr FROM items WHERE title = $1"
		values = append(values, quoted)
	}
 	rows, err := s.db.Query(query, values...)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		item := ItemStat{}
		err = rows.Scan(&item.Date, &item.Base, &item.Quoted)
		if err != nil {
			return nil, err
		}
		items = append(items, item)
	}
	return items, nil
}

func (s *store) GetByDate(date string) ([]Item, error) {
	items := []Item{}
	rows, err := s.db.Query("SELECT title, date, descr FROM items WHERE date = $1", date)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		item := Item{}
		err = rows.Scan(&item.Title, &item.PubDate, &item.Description)
		if err != nil {
			return nil, err
		}
		items = append(items, item)
	}
	return items, nil
}

func (s *store) GetByDateAndTitle(date, title string) (*Item, error) {
	item := &Item{}
	err := s.db.QueryRow("SELECT title, date, descr FROM items WHERE date = $1 AND title = $2 LIMIT 1", date, title).
		Scan(&item.Title, &item.PubDate, &item.Description)
	if err == sql.ErrNoRows {
		return nil, errors.New("not found")
	} else if err != nil {
		return nil, err
	}
	return item, nil
}