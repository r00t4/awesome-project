package lib

import (
	"fmt"
	"strconv"
	"time"
)

type DataController struct {
	clt HttpClient
	store ItemsStore
	url string
}

func NewDataController(clt *HttpClient, store ItemsStore, url string) *DataController {
	return &DataController{
		clt:   *clt,
		store: store,
		url:   url,
	}
}

func (dc *DataController) FetchAndSaveData() error {
	rss, err := dc.clt.GetInfo(dc.url)
	if err != nil {
		return err
	}
	items := []Item{}
	for _, val := range rss.Channel.Item {
		item := Item{
			Title:       val.Title,
			PubDate:     val.PubDate,
			Description: val.Description,
		}
		if k, _ := strconv.Atoi(val.Quant); k != 1 {
			d, err := strconv.ParseFloat(val.Description, 64)
			if err != nil {
				return err
			}
			q, err := strconv.ParseFloat(val.Quant, 64)
			if err != nil {
				return err
			}
			item.Description = fmt.Sprintf("%.6f", d/q)
		}
		items = append(items, item)
	}
	_, err = dc.store.Add(items)
	return err
}

func Scheduler(fn func() error, d time.Duration, sleepTime time.Duration) {
	for range time.Tick(d) {
		cnt := 0
		for i := 0; i < 5; i++ {
			err := fn()
			if err == nil {
				break
			}
			cnt++
			time.Sleep(sleepTime)
		}
		if cnt == 5 {
			fmt.Println("failed to fetch data")
		}
	}
}
