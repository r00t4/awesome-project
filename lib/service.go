package lib

import (
	"fmt"
	"strconv"
	"time"
)

//import "github.com/robfig/cron"

type Service struct {
	store ItemsStore
}

func NewService(store ItemsStore) *Service {
	return &Service{store: store}
}

func (svc *Service) Convert(cmd *ConvertCommand) (*ConvertResponse, error) {
	descQuoted := 1.0
	descBase := 1.0
	t := time.Now()
	timeNow := fmt.Sprintf("%02d.%02d.%d", t.Day(), t.Month(), t.Year())
	if cmd.QuotedCurrency != "KZT" && cmd.QuotedCurrency != "" {
		item, err := svc.store.GetByDateAndTitle(timeNow, cmd.QuotedCurrency)
		if err != nil {
			return nil, err
		}
		descQuoted, err = strconv.ParseFloat(item.Description, 64)
		if err != nil {
			return nil, err
		}
	}
	if cmd.BaseCurrency != "KZT" {
		item, err := svc.store.GetByDateAndTitle(timeNow, cmd.BaseCurrency)
		if err != nil {
			return nil, err
		}
		descBase, err = strconv.ParseFloat(item.Description, 64)
		if err != nil {
			return nil, err
		}
	}
	count := 1.0
	if cmd.Count != 0.0 {
		count = cmd.Count
	}

	return &ConvertResponse{
		ConvertCommand: *cmd,
		Value:          fmt.Sprintf("%.6f", descBase / descQuoted * count),
	}, nil
}

func (svc *Service) GetStatistics(cmd *GetStatisticsCommand) (*GetStatisticsResponse, error) {
	quoted := ""
	base := ""
	if cmd.QuotedCurrency != "KZT" {
		quoted = cmd.QuotedCurrency
	}
	if cmd.BaseCurrency != "KZT" {
		base = cmd.BaseCurrency
	}
	items, err := svc.store.GetByTitles(base, quoted)
	if err != nil {
		return nil, err
	}
	resp := []DayStatistic{}
	for _, val := range items {
		descBase, err := strconv.ParseFloat(val.Base, 64)
		if err != nil {
			return nil, err
		}
		descQuoted, err := strconv.ParseFloat(val.Quoted, 64)
		if err != nil {
			return nil, err
		}
		resp = append(resp, DayStatistic{
			Date:  val.Date,
			Value: fmt.Sprintf("%.6f", descBase / descQuoted),
		})
	}
	return &GetStatisticsResponse{
		GetStatisticsCommand: *cmd,
		Statistics:           resp,
	}, nil
}