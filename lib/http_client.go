package lib

import (
	"encoding/xml"
	"io/ioutil"
	"net/http"
)

type HttpClient struct {
	clt *http.Client
}

func NewHttpClient(clt *http.Client) *HttpClient {
	return &HttpClient{clt: clt}
}

func (c *HttpClient) GetInfo(url string) (*Rss, error) {
	resp, err := c.clt.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	rss := &Rss{}
	err = xml.Unmarshal(body, rss)
	if err != nil {
		return nil, err
	}
	return rss, nil
}