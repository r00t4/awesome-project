package lib

import "encoding/xml"

type Rss struct {
	XMLName xml.Name `xml:"rss"`
	Version string   `xml:"version,attr"`
	Channel Chanel   `xml:"channel"`
}

type Chanel struct {
	Generator   string `xml:"generator"`
	Title       string `xml:"title"`
	Link        string `xml:"link"`
	Description string `xml:"description"`
	Language    string `xml:"language"`
	Copyright   string `xml:"copyright"`
	Item        []Item `xml:"item"`
}

type Item struct {
	Title       string `xml:"title" json:"title"`
	PubDate     string `xml:"pubDate" json:"pub_date"`
	Description string `xml:"description" json:"description"`
	Quant       string `xml:"quant" json:"-"`
	Index       string `xml:"index" json:"-"`
	Change      string `xml:"change" json:"-"`
	Link        string `xml:"link" json:"-"`
}

type ItemStat struct {
	Date   string `json:"date,omitempty"`
	Base   string `json:"base,omitempty"`
	Quoted string `json:"quoted,omitempty"`
}

type ConvertCommand struct {
	BaseCurrency   string  `json:"base_currency"`
	QuotedCurrency string  `json:"quoted_currency,omitempty"`
	Count          float64 `json:"count,omitempty"`
}

type ConvertResponse struct {
	ConvertCommand
	Value string   `json:"value"`
}

type GetStatisticsCommand struct {
	BaseCurrency   string `json:"base_currency"`
	QuotedCurrency string `json:"quoted_currency"`
}

type GetStatisticsResponse struct {
	GetStatisticsCommand
	Statistics []DayStatistic `json:"statistics"`
}

type DayStatistic struct {
	Date  string `json:"date"`
	Value string `json:"value"`
}