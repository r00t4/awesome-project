package lib

import (
	"encoding/json"
	"net/http"
)

type HttpFactory struct {
	svc *Service
}

func NewHttpFactory(svc *Service) *HttpFactory {
	return &HttpFactory{svc: svc}
}

func (fac *HttpFactory) ConvertEndpoint(w http.ResponseWriter, req *http.Request) {
	cmd := &ConvertCommand{}
	err := json.NewDecoder(req.Body).Decode(cmd)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := fac.svc.Convert(cmd)
	if err != nil {
		// пока что не смотрю на статусы ошибок, всегда возвращаю 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	ToJson(resp, w)
}

func (fac *HttpFactory) GetStatisticsEndpoint(w http.ResponseWriter, req *http.Request) {
	cmd := &GetStatisticsCommand{}
	cmd.BaseCurrency = req.URL.Query().Get("base_currency")
	cmd.QuotedCurrency = req.URL.Query().Get("quoted_currency")

	resp, err := fac.svc.GetStatistics(cmd)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	ToJson(resp, w)
}

func ToJson(data interface{}, w http.ResponseWriter) {
	jsonMarshalled, err := json.Marshal(data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonMarshalled)
	return
}