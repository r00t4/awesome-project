package lib

import "testing"

var svcTest *Service

type storeTest struct {}

func (st *storeTest) Add(items []Item) ([]Item, error) {
	return items, nil
}

func (st *storeTest) GetByTitles(base, quoted string) ([]ItemStat, error) {
	return []ItemStat{
		{
			Date:   "21.01.2021",
			Base:   "123",
			Quoted: "321",
		},
		{
			Date:   "22.01.2021",
			Base:   "124",
			Quoted: "322",
		},
	}, nil
}

func (st *storeTest) GetByDateAndTitle(date, title string) (*Item, error) {
	return &Item{
		Title:       title,
		PubDate:     date,
		Description: "418.000000",
		Quant:       "1",
	}, nil
}

func (st *storeTest) GetByDate(date string) ([]Item, error) {
	return []Item{
		{
			Title:       "USD",
			PubDate:     date,
			Description: "418.000000",
			Quant:       "1",
		},
		{
			Title:       "RUB",
			PubDate:     date,
			Description: "5.600000",
			Quant:       "1",
		},
	}, nil
}

func TestService_Convert(t *testing.T) {
	st := storeTest{}
	svcTest = NewService(&st)
	input := &ConvertCommand{
		BaseCurrency:   "USD",
		Count:          0,
	}
	output, err := svcTest.Convert(input)
	if err != nil {
		t.Errorf("Test Failed: %v inputted, recieved error: %v", input, err)
		return
	}
	if output.BaseCurrency != input.BaseCurrency || output.Count != input.Count || output.Value != "418.000000" {
		t.Errorf("Test Failed: %v inputted, recieved: %v", input, output)
	}
}

func TestService_GetStatistics(t *testing.T) {
	st := storeTest{}
	svcTest = NewService(&st)
	input := &GetStatisticsCommand{
		BaseCurrency:   "USD",
		QuotedCurrency: "KZT",
	}
	output, err := svcTest.GetStatistics(input)
	if err != nil {
		t.Errorf("Test Failed: %v inputted, recieved error: %v", input, err)
		return
	}
	if output.BaseCurrency != input.BaseCurrency || output.QuotedCurrency != input.QuotedCurrency {
		t.Errorf("Test Failed: %v inputted, recieved: %v", input, output)
	}
	if len(output.Statistics) != 2 {
		t.Errorf("Test Failed: %v inputted, recieved: %v", input, output)
	}
}