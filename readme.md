# Converter app

### Endpoints

1. **POST** *localhost:8090/convert*  
Request for converter.  
Example body:
>{  
>&nbsp;&nbsp;&nbsp;&nbsp;"base_currency": "USD", // *(Базовая валюта), required*    
>&nbsp;&nbsp;&nbsp;&nbsp;"quoted_currency": "RUB", // *(Котируемая валюта), not required, default value: KZT*  
>&nbsp;&nbsp;&nbsp;&nbsp;"count": "2.0", // *(Количество), not required, default value: 1.0*  
>} 


2. **GET** *localhost:8090/statistics*  
Request for getting changing history of course for requested currency.
Example parameters in url *(ex. url?par1=val1&par2=val2)*:
>  
>&nbsp;&nbsp;"base_currency": "USD" // *(Базовая валюта), required*    
>&nbsp;&nbsp;"quoted_currency": "RUB" // *(Котируемая валюта), not required, default value: KZT*

***
Build Docker image with given Dockerfile:
> docker build -t awesome-project .

Then simply run it
> docker run -dit -p 8090:8090 awesome-project