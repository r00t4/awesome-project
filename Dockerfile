FROM golang:1.14.5-alpine AS builder

RUN apk update && apk add --no-cache git
RUN echo "[url \"git@gitlab.com:\"]\n\tinsteadOf = https://gitlab.com/" >> /root/.gitconfig

ADD . /go/src/gitlab.com/r00t4/awesome-project
WORKDIR $GOPATH/src/gitlab.com/r00t4/awesome-project

CMD go get -d -v && go run main.go
