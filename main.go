package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/awesome-project/lib"
	"net/http"
	"time"
)

func main() {
	connStr := "postgres://postgres:Newpass12@database-central.c9vdzz9niagi.eu-central-1.rds.amazonaws.com:5432/shaker"
	fmt.Println("trying to connect to postgres...")
	store, err := lib.NewItemsStore(connStr)
	if err != nil {
		fmt.Println(err)
		return
	}
	clt := &http.Client{}
	httpClt := lib.NewHttpClient(clt)

	dController := lib.NewDataController(httpClt, store, "https://nationalbank.kz/rss/rates_all.xml?switch=kazakh")
	err = dController.FetchAndSaveData()
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("running cron job...")
	go lib.Scheduler(dController.FetchAndSaveData, time.Hour * 24, time.Minute)

	fmt.Println("running server...")
	svc := lib.NewService(store)
	fac := lib.NewHttpFactory(svc)

	r := mux.NewRouter()
	r.HandleFunc("/convert", fac.ConvertEndpoint).Methods("POST")
	r.HandleFunc("/statistics", fac.GetStatisticsEndpoint).Methods("GET")
	http.ListenAndServe(":8090", r)
}
